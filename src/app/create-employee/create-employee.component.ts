import {Component, Input, OnInit} from '@angular/core';
import {NgbActiveModal, NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {EmployeeDataService} from "../service/employee-data.service";
import {Employee} from "../Employee";
import {Qualification} from "../Qualification";
import {QualificationService} from "../service/qualification.service";
import {Observable} from "rxjs";

@Component({
  selector: 'app-create-employee',
  templateUrl: './create-employee.component.html',
  styleUrls: ['./create-employee.component.css']
})
export class CreateEmployeeComponent implements OnInit {

  public newEmployee: Employee;
  public selectedQuali: Qualification;
  public qualificationsToAdd: Qualification[] = [];


  constructor(public activeModal: NgbActiveModal, private employeeDataService: EmployeeDataService,
              private qualificationService: QualificationService) {
    this.newEmployee = new Employee();
    this.selectedQuali = new Qualification();
  }

  ngOnInit(): void {
  }

  abort() {
    this.activeModal.close('Close click')
  }

  async add(): Promise<Employee | void> {
    await this.employeeDataService.addEmployee(this.newEmployee, this.qualificationsToAdd)
    this.newEmployee = new Employee()
    this.qualificationsToAdd = []
  }

  get qualification(): Observable<Qualification[]> {
    return this.qualificationService.qualification$
  }

  addQualificationToEmployee() {
    if (!this.qualificationsToAdd.includes(this.selectedQuali) && this.selectedQuali.designation != undefined) {
      this.qualificationsToAdd.push(this.selectedQuali)
    }
    return
  }

  deleteQualificationFromEmployee(qualification: Qualification){
    this.qualificationsToAdd = this.qualificationsToAdd.filter(e => e!== qualification );
  }

  containsQualification(array: Qualification[], q: Qualification) : boolean {
    return array.some(qual => qual.designation === q.designation);
  }

}
