import { Component, OnInit } from '@angular/core';
import {NgbActiveModal, NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {QualificationService} from "../service/qualification.service";
import {Qualification} from "../Qualification";
import {Employee} from "../Employee";

@Component({
  selector: 'app-create-qualification',
  templateUrl: './create-qualification.component.html',
  styleUrls: ['./create-qualification.component.css']
})
export class CreateQualificationComponent implements OnInit {

  public newQualification: Qualification;

  constructor(public activeModal: NgbActiveModal, private QualificationService: QualificationService) {
    this.newQualification = new Qualification();
  }

  ngOnInit(): void {
  }

  abort(){
    this.activeModal.close('Close click')
  }


  async add(): Promise<Qualification|void> {
    await this.QualificationService.addQualification(this.newQualification)
    this.newQualification = new Qualification()
  }

}
