import { TestBed } from '@angular/core/testing';

import { BearerTokenExistsGuard } from './bearer-token-exists.guard';

describe('BearerTokenExistsGuardGuard', () => {
  let guard: BearerTokenExistsGuard;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    guard = TestBed.inject(BearerTokenExistsGuard);
  });

  it('should be created', () => {
    expect(guard).toBeTruthy();
  });
});
