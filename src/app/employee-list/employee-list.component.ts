import { Component, OnInit } from '@angular/core';
import {CreateEmployeeComponent} from "../create-employee/create-employee.component"
import {EmployeeDataService} from "../service/employee-data.service";
import {Observable} from "rxjs";
import {Employee} from "../Employee";
import {NgbActiveModal, NgbModal} from '@ng-bootstrap/ng-bootstrap'
import {EditEmployeeComponent} from "../edit-employee/edit-employee.component";
import {Qualification} from "../Qualification";
import {QualificationService} from "../service/qualification.service";

@Component({
  selector: 'app-employee-list',
  templateUrl: './employee-list.component.html',
  styleUrls: ['./employee-list.component.css']
})
export class EmployeeListComponent implements OnInit {

  public filterId?: number;
  public filterQualification?: Qualification;

  constructor(private employeeDataService: EmployeeDataService, private modalService: NgbModal, private qualificationService: QualificationService) { }

  ngOnInit(): void {
  }

  get employees(): Observable<Employee[]>{
    return this.employeeDataService.employees$
  }

  get qualification(): Observable<Qualification[]>{
    return this.qualificationService.qualification$
  }

  async delete(id: number): Promise<void>{
    await this.employeeDataService.deleteEmployee(id);
  }

  openAddEmployee() {
    const modalRef = this.modalService.open(CreateEmployeeComponent)
  }

  openEditEmployee(employee: Employee) {
    const modalRef = this.modalService.open(EditEmployeeComponent)
    modalRef.componentInstance.editEmployee = employee
  }

  containsQualification(array: Qualification[], q: Qualification) : boolean {
    return array.some(qual => qual.designation === q.designation);
  }

}
