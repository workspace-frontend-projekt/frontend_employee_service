import {Component, Input, OnInit} from '@angular/core';
import {HttpCommunicationService} from "../service/http-communication.service";

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  username!: string;
  password!: string;
  noKey: string = "NO KEY!";
  token!: string;

  constructor(private service: HttpCommunicationService) {
    console.log("ICH BIN DER LOGIN SERVICE")
  }

  ngOnInit(): void {
  }

  getToken() {
    this.service.getToken(this.username, this.password);
  }
}
