import {Component, ElementRef, OnInit, ViewChild} from '@angular/core';

@Component({
  selector: 'app-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.css']
})
export class MainComponent implements OnInit {
  employeeSelected: boolean = true;
  qualificationSelected: boolean = false;
  @ViewChild('employeeSelect') employeeSelect!: ElementRef;
  @ViewChild('qualificationSelect') qualificationSelect!: ElementRef;

  constructor() {}

  ngOnInit(): void {
  }

  setEmployee() {
    this.qualificationSelect.nativeElement.classList.remove("active");
    this.employeeSelect.nativeElement.classList.add("active");
    this.employeeSelected = true;
    this.qualificationSelected = false;
  }

  setQualification() {
    this.employeeSelect.nativeElement.classList.remove("active");
    this.qualificationSelect.nativeElement.classList.add("active");
    this.employeeSelected = false;
    this.qualificationSelected = true;
  }

  checkForClass(classString: string, element: ElementRef) {
    if (element.nativeElement.classList.contains(classString)) {
      return true;
    }
    return false;
  }
}
