import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {AppComponent} from "./app.component";
import {LoginComponent} from "./login/login.component";
import {MainComponent} from "./main/main.component";
import {BearerTokenExistsGuard} from "./bearer-token-exists.guard";


export const routes: Routes = [
  { path: '', component: LoginComponent },
  { path: 'app', component: MainComponent, canActivate: [BearerTokenExistsGuard] }
];

@NgModule({
  imports: [ RouterModule.forRoot(routes) ],
  exports: [ RouterModule ]
})
export class AppRoutingModule {}
