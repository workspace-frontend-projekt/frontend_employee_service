import { Component, OnInit } from '@angular/core';
import {CreateQualificationComponent} from "../create-qualification/create-qualification.component";
import {QualificationService} from "../service/qualification.service";
import {Observable} from "rxjs";
import {NgbActiveModal, NgbModal} from '@ng-bootstrap/ng-bootstrap'
import {Qualification} from "../Qualification";

@Component({
  selector: 'app-qualification-list',
  templateUrl: './qualification-list.component.html',
  styleUrls: ['./qualification-list.component.css']
})
export class QualificationListComponent implements OnInit {

  constructor(private qualificationService: QualificationService, private modalService: NgbModal) { }

  ngOnInit(): void {
  }

  get qualification(): Observable<Qualification[]>{
    return this.qualificationService.qualification$
  }

  async delete(designation: string): Promise<void>{
    await this.qualificationService.deleteQualification(designation);
  }

  openAddQualification() {
    const modalRef = this.modalService.open(CreateQualificationComponent)
  }

}

