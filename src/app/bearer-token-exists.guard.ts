import { Injectable } from '@angular/core';
import {ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot, UrlTree} from '@angular/router';
import { Observable } from 'rxjs';
import {CookieService} from "ngx-cookie-service";

@Injectable({
  providedIn: 'root'
})
export class BearerTokenExistsGuard implements CanActivate {

  constructor(private cookie: CookieService, private router: Router) {
  }
  canActivate(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): boolean {
    let exists = this.cookie.get("bearer") != undefined && this.cookie.get("bearer") != '';
    if (!exists) {
      this.router.navigate([])
    }
    return exists
  }
}
