import { Injectable } from '@angular/core';
import {Qualification} from "../Qualification";
import {HttpCommunicationService} from "../service/http-communication.service";
import {BehaviorSubject, Observable} from "rxjs";

@Injectable({
  providedIn: 'root'
})
export class QualificationService {
  private qualifications: BehaviorSubject<Qualification[]> = new BehaviorSubject<Qualification[]>([]);
  public readonly qualification$: Observable<Qualification[]> = this.qualifications.asObservable();

  constructor(private httpCommunicationService: HttpCommunicationService) {
    this.fetchData()
  }

  async fetchData(): Promise<void> {
    this.qualifications.next(await this.httpCommunicationService.getQualifications());
  }

  async deleteQualification(designation: string): Promise<void>{
    await this.httpCommunicationService.deleteQualification(designation);
    await this.fetchData()
  }

  async addQualification(qualification: Qualification): Promise<Qualification|void>{
    await this.httpCommunicationService.addQualification(qualification)
    await this.fetchData()
  }
}



