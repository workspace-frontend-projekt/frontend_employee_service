
import { Injectable } from '@angular/core';
import {Employee} from "../Employee";
import {HttpCommunicationService} from "../service/http-communication.service";
import {BehaviorSubject, Observable} from "rxjs";
import {Qualification} from "../Qualification";

@Injectable({
  providedIn: 'root'
})
export class EmployeeDataService {
  private employees: BehaviorSubject<Employee[]> = new BehaviorSubject<Employee[]>([]);
  public readonly employees$: Observable<Employee[]> = this.employees.asObservable();


  constructor(private httpCommunicationService: HttpCommunicationService) {
    this.fetchData()
  }


  async fetchData(): Promise<void> {
    let employeeList: Employee[] = await this.httpCommunicationService.getEmployees();
    for (const value of employeeList) {
      value.skillSet = await this.httpCommunicationService.getQualificationsOfEmployee(value.id!);
    }
    this.employees.next(employeeList);
  }

  async deleteEmployee(id:number): Promise<void>{
    await this.httpCommunicationService.deleteEmployee(id);
    await this.fetchData();
  }

  async addEmployee(employee: Employee, toAdd: Qualification[]): Promise<Employee|void>{
    await this.httpCommunicationService.addEmployee(employee);
    let employeeList: Employee[] = await this.httpCommunicationService.getEmployees();
    let newEmployeeId: number = employeeList[employeeList.length-1].id!
    await toAdd.forEach(async value => await this.addQualificationToEmployee(newEmployeeId!, value))
    await this.fetchData();
  }

  async editEmployee(employee: Employee, toAdd: Qualification[], toDelete: Qualification[]): Promise<Employee|void> {
    await this.httpCommunicationService.editEmployee(employee)
    await toAdd.forEach(async value => await this.addQualificationToEmployee(employee.id!,value))
    await toDelete.forEach(async value => await this.deleteQualificationFromEmployee(employee.id!, value))
  }

  async addQualificationToEmployee(id: number, qualification: Qualification): Promise<Qualification|void>{
    await this.httpCommunicationService.addQualificationToEmployee(id,qualification)
  }

  async deleteQualificationFromEmployee(id: number, qualification: Qualification): Promise<Qualification|void>{
    await this.httpCommunicationService.deleteQualificationFromEmployee(id,qualification)
  }

}
