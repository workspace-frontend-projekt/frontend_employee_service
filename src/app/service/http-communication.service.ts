import {Injectable, OnInit} from '@angular/core';
import {Employee} from "../Employee";
import {Qualification} from "../Qualification";
import {EmployeeQualification} from "../EmployeeQualification";
import {HttpClient, HttpHeaders, HttpParams} from "@angular/common/http";
import {firstValueFrom, Observable} from "rxjs";
import {CookieService} from "ngx-cookie-service";
import {Router} from "@angular/router";

@Injectable({
  providedIn: 'root'
})
export class HttpCommunicationService {
  constructor(private http: HttpClient, private cookie: CookieService, private router: Router) {
  }

  async getToken(username: string, password: string) {
    let httpParams = new HttpParams()
      .set("grant_type", "password")
      .set("client_id", "employee-management-service")
      .set("username", username)
      .set("password", password);
    let headers = new HttpHeaders().set('Content-Type', 'application/x-www-form-urlencoded')

    await this.http.post("http://authproxy.szut.dev", httpParams, {
      headers: headers
    }).subscribe((data: any) => {
      this.cookie.set('bearer', data["access_token"]);
      this.router.navigateByUrl("/app");
    })
  }

  async getEmployees(): Promise<Employee[]> {
    return await firstValueFrom(this.http.get<Employee[]>('/backend/employees', {
      headers: new HttpHeaders()
        .set('Content-Type', 'application/json')
        .set('Authorization', `Bearer ${this.cookie.get('bearer')}`)
    }))
  }

  async getQualificationsOfEmployee(id: number): Promise<Qualification[]> {
    return await firstValueFrom(this.http.get<EmployeeQualification>('/backend/employees/' + id + '/qualifications',
      {
        headers: new HttpHeaders()
          .set('Content-Type', 'application/json')
          .set('Authorization', `Bearer ${this.cookie.get('bearer')}`)
      })).then(s => {
      return s.skillSet!
    }, f => {
      return <Qualification[]>[]
    });
  }

  async deleteEmployee(id: number): Promise<void> {
    await firstValueFrom(this.http.delete('/backend/employees/' + id,
      {
        headers: new HttpHeaders()
          .set('Content-Type', 'application/json')
          .set('Authorization', `Bearer ${this.cookie.get('bearer')}`)
      }))

  }

  async addEmployee(employee: Employee): Promise<Employee | null> {
    let body: string = JSON.stringify({
      "lastName": employee.lastName,
      "firstName": employee.firstName,
      "street": employee.street,
      "postcode": employee.postcode,
      "city": employee.city,
      "phone": employee.phone
    })
    await firstValueFrom(this.http.post('/backend/employees', body,
      {
        headers: new HttpHeaders()
          .set('Content-Type', 'application/json')
          .set('Authorization', `Bearer ${this.cookie.get('bearer')}`)
      }))
    return employee
  }

  async editEmployee(employee: Employee): Promise<Employee | null> {
    let body: string = JSON.stringify({
      "lastName": employee.lastName,
      "firstName": employee.firstName,
      "street": employee.street,
      "postcode": employee.postcode,
      "city": employee.city,
      "phone": employee.phone
    })
    await firstValueFrom(this.http.put('/backend/employees/' + employee.id, body,
      {
        headers: new HttpHeaders()
          .set('Content-Type', 'application/json')
          .set('Authorization', `Bearer ${this.cookie.get('bearer')}`)
      }))
    return employee
  }

  async addQualificationToEmployee(id: number, qualification: Qualification): Promise<Qualification | null> {
    let body: string = JSON.stringify({
      "designation": qualification.designation
    })
    await firstValueFrom(this.http.post('/backend/employees/' + id + '/qualifications', body,
      {
        headers: new HttpHeaders()
          .set('Content-Type', 'application/json')
          .set('Authorization', `Bearer ${this.cookie.get('bearer')}`)
      }))
    return qualification
  }

  async deleteQualificationFromEmployee(id: number, qualification: Qualification): Promise<Qualification | null> {
    let body: string = JSON.stringify({
      "designation": qualification.designation
    })
    await firstValueFrom(this.http.delete('/backend/employees/' + id + '/qualifications',
      {
        headers: new HttpHeaders()
          .set('Content-Type', 'application/json')
          .set('Authorization', `Bearer ${this.cookie.get('bearer')}`),
        body: body
      }))
    return qualification
  }


  async getQualifications(): Promise<Qualification[]> {
    return await firstValueFrom(this.http.get<Qualification[]>('/backend/qualifications',
      {
        headers: new HttpHeaders()
          .set('Content-Type', 'application/json')
          .set('Authorization', `Bearer ${this.cookie.get('bearer')}`)
      }))
  }

  async deleteQualification(designation: string): Promise<void> {
    let body: string = JSON.stringify({
      "designation": designation
    })
    await firstValueFrom(this.http.delete('/backend/qualifications/',
      {
        headers: new HttpHeaders()
          .set('Content-Type', 'application/json')
          .set('Authorization', `Bearer ${this.cookie.get('bearer')}`),
        body: body
      }))

  }

  async addQualification(qualification: Qualification) : Promise<Qualification|null>{
    let body: string = JSON.stringify({
      "designation": qualification.designation
    })
    await firstValueFrom(this.http.post('/backend/qualifications',body,
      {headers: new HttpHeaders()
          .set('Content-Type', 'application/json')
          .set('Authorization', `Bearer ${this.cookie.get('bearer')}`)
      }))
    return qualification
  }
}
