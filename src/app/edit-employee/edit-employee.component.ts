import {Component, Input, OnInit} from '@angular/core';
import {NgbActiveModal, NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {EmployeeDataService} from "../service/employee-data.service";
import {QualificationService} from "../service/qualification.service";
import {Employee} from "../Employee";
import {Observable} from "rxjs";
import {Qualification} from "../Qualification";


@Component({
  selector: 'app-edit-employee',
  templateUrl: './edit-employee.component.html',
  styleUrls: ['./edit-employee.component.css']
})
export class EditEmployeeComponent implements OnInit {

  public editEmployee: Employee;
  public selectedQuali: Qualification;
  private qualificationsToAdd: Qualification[] = [];
  public qualificationsToDelete: Qualification[] = [];

  constructor(public activeModal: NgbActiveModal, private employeeDataService: EmployeeDataService,
              private qualificationService: QualificationService) {
    this.editEmployee = new Employee()
    this.selectedQuali = new Qualification()
  }

  ngOnInit(): void {
  }

  async abort(){
    await this.employeeDataService.fetchData()
    this.activeModal.close('Close click')
    this.qualificationsToAdd = []
    this.qualificationsToDelete = []
  }

  async safeEdit(): Promise<Employee|void> {
    await this.employeeDataService.editEmployee(this.editEmployee,
      this.qualificationsToAdd,this.qualificationsToDelete)
    this.qualificationsToAdd = []
    this.qualificationsToDelete = []
  }

  get qualification(): Observable<Qualification[]>{
    return this.qualificationService.qualification$
  }

  addQualificationToEmployee() {
    if(this.qualificationsToDelete.includes(this.selectedQuali)){
      this.qualificationsToDelete = this.qualificationsToDelete.filter(e => e!== this.selectedQuali );
      if (!this.editEmployee.skillSet?.includes(this.selectedQuali)){
        this.editEmployee.skillSet?.push(this.selectedQuali)
      }
      return
    }
    if (!this.qualificationsToAdd.includes(this.selectedQuali)){
      this.qualificationsToAdd.push(this.selectedQuali)
      if (!this.editEmployee.skillSet?.includes(this.selectedQuali)){
        this.editEmployee.skillSet?.push(this.selectedQuali)
      }
    }

  }

  deleteQualificationFromEmployee(qualification: Qualification){
    if(this.qualificationsToAdd.includes(this.selectedQuali)){
      this.qualificationsToAdd = this.qualificationsToAdd.filter(e => e!== this.selectedQuali );
    }
    else if (!this.qualificationsToDelete.includes(qualification) && this.editEmployee.skillSet?.includes(qualification)){
      this.qualificationsToDelete.push(qualification)
    }
    this.editEmployee.skillSet?.splice(this.editEmployee.skillSet?.indexOf(qualification),1)
  }

  containsQualification(array: Qualification[], q: Qualification) : boolean {
    return array.some(qual => qual.designation === q.designation);
  }

}
